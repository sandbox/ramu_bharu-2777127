CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration

INTRODUCTION
------------
This module used to clone commerce product displays from the existing product displays.

REQUIREMENTS
------------
 * [Entity](https://drupal.org/project/entity)
 * [Views](https://drupal.org/project/views)
 * [Commerce](https://drupal.org/project/commerce)
 * [Inline Conditions ](https://drupal.org/project/inline_conditions)
 * [Entity Reference ](https://drupal.org/project/entityreference)
 * [Inline Entity Form](https://drupal.org/project/inline_entity_form)

INSTALLATION
------------
1. Check requirements section first.
2. Enable the module.
https://www.drupal.org/documentation/install/modules-themes/modules-7

CONFIGURATION
------------

* To clone the commerce product displays, go to 
'admin/clone-product-display' and click on 'clone' button.
* Here you can enter the title, sku and image for the products and submit the form. It will clone the product display with new title and SKU with all parent product display attributes.


